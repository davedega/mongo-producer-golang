package main

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	mgo "gopkg.in/mgo.v2"

	"github.com/goamz/goamz/aws"
	"github.com/goamz/goamz/sqs"
)

var (
	database                         string
	debug                            bool
	betweenConsumeWait               float64
	maxErrorsAllowed                 float64
	maxTimeWait                      float64
	timeWaitIncrementalRate          float64
	incrementaTimeBetweenConsumeWait float64
	queue                            *sqs.Queue
	errParsing                       error
	//For Mongo
	mongoSession            *mgo.Session
	detailCollection        *mgo.Collection
	nodeSummaryCollection   *mgo.Collection
	userSummaryCollection   *mgo.Collection
	deviceSummaryCollection *mgo.Collection
	errMongo                error
)

const (
	accessKey = "xxx"
	secretKey = "xx+xxxxx"
	queueName = "https://sqs.us-east-1.amazonaws.com/xx/xx"
)

type Body struct {
	Time        time.Time   `json:"time"`
	User        User        `json:"user"`
	Application Application `json:"application"`
	Device      Device      `json:"device"`
	ServerName  string      `json:"servername"`
	Cdn         Cdn         `json:"cdn"`
	Operator    Operator    `json:"operator"`
}
type User struct {
	UserName    string `json:"username"`
	Userid      int    `json:"userid"`
	Hks         string `json:"hks"`
	Userhash    string `json:"userhash"`
	Email       string `json:"email"`
	Suscripcion int    `json:"suscription"`
	Region      string `json:"region"`
}
type Application struct {
	Platform string `json:"platform"`
	Version  string `json:"version"`
}
type Device struct {
	Ip       string `json:"ip"`
	Kind     string `json:"kind"`
	Brand    string `json:"brand"`
	Model    string `json:"model"`
	Os       string `json:"os"`
	DeviceID string `json:"deviceid"`
}
type Cdn struct {
	Node         string `json:"node"`
	XCache       string `json:"X-Cache"`
	XCacheRemote string `json:"X-Cache-Remote"`
	XCacheKey    string `json:"X-Cache-Key"`
}
type Operator struct {
	ContentID   string `json:"contentid"`
	OperatorKey string `json:"operatorKey"`
	Status      int    `json:"status"`
	Request     string `json:"request"`
	Response    string `json:"response"`
	Stacktrace  string `json:"stacktrace"`
	Msg         string `json:"msg"`
}

func main() {
	log.Printf("=== Mongo Producer === \n")
	initialize()
	runMongoProducer()
}

func runMongoProducer() {
	msgs := getMessages()

	var _bodies []string
	var messagesToDelete []sqs.Message

	if len(msgs.Messages) > 0 {
		log.Printf("=== MESSES IN QUEUE === \n")
		for i := range msgs.Messages {
			_bodies = append(_bodies, msgs.Messages[i].Body)
			messagesToDelete = append(messagesToDelete, sqs.Message{
				msgs.Messages[i].MessageId,
				msgs.Messages[i].Body,
				"", msgs.Messages[i].ReceiptHandle, nil, nil, ""})
		}
		processMessages(_bodies, messagesToDelete)
	} else {
		log.Printf("No messages in queue \n ======== NO_MSGS =========\n\n")
		if incrementaTimeBetweenConsumeWait < betweenConsumeWait*10 {
			incrementaTimeBetweenConsumeWait = incrementaTimeBetweenConsumeWait + betweenConsumeWait*.2
		}
		consumeTimeOut(incrementaTimeBetweenConsumeWait)
	}
}

func processMessages(bodies []string, mensajesParaBorrar []sqs.Message) {
	log.Printf("numero de mensajes: %d \n", len(bodies))

	for i := range bodies {
		log.Printf("Formatting string with index... %d \n", i)
		bodies[i] = strings.Replace(bodies[i], "[", "", -1)
		bodies[i] = strings.Replace(bodies[i], "]", "", -1)
		log.Printf("String formatted \n")
		bytes := []byte(bodies[i])
		var unMsj Body
		json.Unmarshal(bytes, &unMsj)
		log.Printf("Struct ready to validation ...\n")
		//TODO validar estructura de los mensajes
		if validateMessage(unMsj) {
			if saveMessage(unMsj, detailCollection) {
				if updateSummaries(unMsj, nodeSummaryCollection, userSummaryCollection, deviceSummaryCollection) {
				}
			}
		}
	}
	deleteMessages(mensajesParaBorrar, queue)
}

func getDialInfo() *mgo.DialInfo {
	if strings.EqualFold(database, "local") {
		log.Printf("============== MONGO LOCAL ==============")

		dialInfo := &mgo.DialInfo{
			Addrs: []string{
				"localhost"},
			Database: "dashboard",
		}
		return dialInfo
	} else {
		log.Printf("============== MONGO PRODUCTION ==============")

		dialInfo := &mgo.DialInfo{
			Addrs: []string{
				"mongo-dashboard-rs-01.dashboard.amx:27000",
				"mongo-dashboard-rs-02.dashboard.amx:27000",
				"mongo-dashboard-rs-03.dashboard.amx:27000"},
			Database: "xx",
			Username: "xx",
			Password: "xx",
			DialServer: func(addr *mgo.ServerAddr) (net.Conn, error) {
				return tls.Dial("tcp", addr.String(), &tls.Config{InsecureSkipVerify: true})
			},
			Timeout: time.Second * 10,
		}
		return dialInfo
	}
}

func getMessages() *sqs.ReceiveMessageResponse {
	receiveMessageNum := 10
	auth := aws.Auth{AccessKey: accessKey, SecretKey: secretKey}
	mySqs := sqs.New(auth, aws.USEast)
	queue = &sqs.Queue{mySqs, queueName}

	resp, err := queue.ReceiveMessage(receiveMessageNum)
	if err != nil {
		log.Println(err)
	}
	return resp
}

func validateMessage(message Body) bool {
	var pasa bool
	pasa = true
	if strings.Compare("", message.Time.String()) == 0 {
		pasa = false
		log.Printf("Empty time \n")
	}
	if strings.Compare("", message.User.UserName) == 0 {
		pasa = false
		log.Printf("Empty User.UserName \n")
	}
	if strings.Compare("", message.User.Email) == 0 {
		pasa = false
		log.Printf("Empty User.Email \n")
	}
	// if strings.Compare("", message.User.Userid) == 0 {
	// 	pasa = false
	// 	log.Printf("Empty Userid \n")
	// }
	if strings.Compare("", message.User.Hks) == 0 {
		pasa = false
		log.Printf("Empty User.Hks  \n")
	}
	if strings.Compare("", message.User.Userhash) == 0 {
		pasa = false
		log.Printf("Empty User.Userhash  \n")
	}
	// if strings.Compare("", message.User.Suscripcion) == 0 {
	// 	pasa = false
	// 	log.Printf("Empty Suscripcion  \n")
	// }
	if strings.Compare("", message.User.Region) == 0 {
		pasa = false
		log.Printf("Empty User.Region \n")
	}
	if strings.Compare("", message.Application.Platform) == 0 {
		pasa = false
		log.Printf("Empty Application.Platform \n")
	}
	if strings.Compare("", message.Application.Version) == 0 {
		pasa = false
		log.Printf("Empty Application.Version  \n")
	}
	if strings.Compare("", message.Device.Brand) == 0 {
		pasa = false
		log.Printf("Empty Device.Brand \n")
	}
	if strings.Compare("", message.Device.Ip) == 0 {
		pasa = false
		log.Printf("Empty Device.Ip  \n")
	}
	if strings.Compare("", message.Device.Kind) == 0 {
		pasa = false
		log.Printf("Empty Device.Kind \n")
	}
	if strings.Compare("", message.Device.Model) == 0 {
		pasa = false
		log.Printf("Empty Device.Model \n")
	}
	if strings.Compare("", message.Device.Os) == 0 {
		pasa = false
		log.Printf("Empty Device.Os  \n")
	}
	if strings.Compare("", message.Device.DeviceID) == 0 {
		pasa = false
		log.Printf("Empty Device.DeviceID  \n")
	}
	if strings.Compare("", message.Cdn.Node) == 0 {
		pasa = false
		log.Printf("Empty Cdn.Node  \n")
	}
	if strings.Compare("", message.Cdn.XCache) == 0 {
		pasa = false
		log.Printf("Empty Cdn.XCache \n")
	}
	// if strings.Compare("", message.Cdn.XCacheKey) == 0 {
	// 	pasa = false
	// 	log.Printf("Empty Cdn.XCacheKey \n")
	// }
	// if strings.Compare("", message.Cdn.XCacheRemote) == 0 {
	// 	pasa = false
	// 	log.Printf("Empty Cdn.XCacheRemote \n")
	// }
	if strings.Compare("", message.ServerName) == 0 {
		pasa = false
		log.Printf("Empty ServerName  \n")
	}
	if strings.Compare("", message.Operator.OperatorKey) == 0 {
		pasa = false
		log.Printf("Empty Operator.OperatorKey \n")
	}
	// if mensaje.Operator.Status == nil {
	// 	pasa = false
	// }
	if strings.Contains(message.Operator.OperatorKey, "pgm") {
		if strings.Compare("", message.Operator.Request) == 0 {
			pasa = false
			log.Printf("Empty Operator.Request for pgm or live_pgm  \n")
		}
		if strings.Compare("", message.Operator.Response) == 0 {
			pasa = false
			log.Printf("Empty Operator.Response for pgm or live_pgm \n")
		}
	}
	// if strings.Compare("", message.Operator.Stacktrace) == 0 {
	// 	pasa = false
	// 	log.Printf("Empty Operator.Stacktrace \n")
	// }
	// if strings.Compare("", message.Operator.Msg) == 0 {
	// 	pasa = false
	// 	log.Printf("Empty Operator.Msg \n")
	// }
	return pasa
}

func saveMessage(message Body, c *mgo.Collection) bool {
	log.Printf("Saving detail...")
	errDetail := c.Insert(message)
	if errDetail != nil {
		log.Fatal(errDetail)
		return false
	} else {
		log.Printf("Save detail OK!")
		return true
	}
}

func updateSummaries(mensaje Body, nodeSum *mgo.Collection, userSum *mgo.Collection, deviceSum *mgo.Collection) bool {

	log.Println("Updating Summaries ")

	var failedEvent int
	if mensaje.Operator.Status == 0 {
		failedEvent = 1
	} else {
		failedEvent = 0
	}
	node := mensaje.Cdn.Node
	if node == "default" {
		log.Println("Invalid message, does not contain node value \n ")
		return true
	} else {
		ip, err := parseIP(node)
		if err != nil {
			log.Fatal(err)
			return true
		}
		updateNodeSummary(nodeSum, node, ip, failedEvent)
		updateUserSummary(userSum, mensaje, node, ip, failedEvent)
		updateDeviceSummary(deviceSum, mensaje, failedEvent)
		return true
	}
}

func updateNodeSummary(nodeSummaryCollection *mgo.Collection, node string, ip string, failedEvent int) {
	log.Println("Updating node summary...")

	t := time.Now()
	type M map[string]interface{}
	if cool, errUp := nodeSummaryCollection.Upsert(
		M{
			"node": node,
			"date": t.Truncate(time.Hour),
		},
		M{
			"$setOnInsert": M{"ip": ip},
			"$inc":         M{"failedEvents": failedEvent, "totalEvents": 1},
		},
	); errUp != nil {
		log.Println("Failed: ", errUp.Error())
	} else {
		log.Println("OK! ", cool.Updated)
	}
}

func updateUserSummary(userSummaryCollection *mgo.Collection, message Body, node string, ip string, failedEvent int) {
	log.Println("Updating user summary... ")
	t := time.Now()
	type M map[string]interface{}
	if cool, errUp := userSummaryCollection.Upsert(
		M{
			"node":   node,
			"date":   t.Truncate(time.Hour),
			"userId": message.User.Userid,
		},
		M{
			"$inc": M{
				"failedEvents": failedEvent,
				"totalEvents":  1,
			},
			"$setOnInsert": M{
				"username":    message.User.UserName,
				"userEmail":   message.User.Email,
				"userRegion":  message.User.Region,
				"suscription": message.User.Suscripcion,
			},
		},
	); errUp != nil {
		log.Println("Failed: ", errUp.Error())
	} else {
		log.Println("OK! ", cool.Updated)
	}
}

func updateDeviceSummary(deviceSummaryCollection *mgo.Collection, message Body, failedEvent int) {
	log.Println("Updating device summary...  ")
	t := time.Now()
	type M map[string]interface{}
	if cool, errUp := deviceSummaryCollection.Upsert(
		M{
			"node":     message.Cdn.Node,
			"userId":   message.User.Userid,
			"deviceId": message.Device.DeviceID,
			"date":     t.Truncate(time.Hour),
		},
		M{
			"$inc": M{
				"failedEvents": failedEvent,
				"totalEvents":  1,
			},
			"$setOnInsert": M{
				"deviceKind":  message.Device.Kind,
				"deciveBrand": message.Device.Brand,
				"deviceModel": message.Device.Model,
				"deviceOs":    message.Device.Os,
				"ip":          message.Device.Ip,
				"userEmail":   message.User.Email,
				"userRegion":  message.User.Region,
				"suscription": message.User.Suscripcion,
			},
		},
	); errUp != nil {
		log.Println("Failed: ", errUp.Error())
	} else {
		log.Println("OK ! \n\n", cool.Updated)
	}
}

func deleteMessages(losMensajes []sqs.Message, elQueue *sqs.Queue) bool {
	log.Printf("Deleting messages... ")
	if resp, err := elQueue.DeleteMessageBatch(losMensajes); err != nil {
		log.Println("Batch Delete Failed: ", err.Error())
	} else {
		log.Println("Batch Delete Successful: ", resp)
		log.Printf("======== FIN ========\n\n")
		incrementaTimeBetweenConsumeWait = 10
		consumeTimeOut(incrementaTimeBetweenConsumeWait)
	}
	return true
}

// Esta funcion extrae la ip del nombre del nodo y la regresa como string
func parseIP(urlStr string) (string, error) {
	i := strings.IndexByte(urlStr, '.')
	if i < 0 {
		return "", errors.New("invalid urlStr")
	}
	urlStr = strings.Replace(urlStr[:i], "-", ".", -1)
	return strings.TrimLeft(urlStr, "abcdefghijklmnopqrstuvwxyz"), nil
}

func initialize() {
	database = "localhost"
	debug = false
	betweenConsumeWait = 2000
	maxErrorsAllowed = 10
	maxTimeWait = 5000
	timeWaitIncrementalRate = 0.2

	argCount := os.Args[1:]

	for i, a := range argCount {
		log.Printf("argument index :%v ", i)

		if strings.Contains(a, "--db=") {
			log.Printf("Reading database argument: %s", a)
			database = strings.Replace(a, "--db=", "", -1)
			if strings.EqualFold(a, "") {
				log.Printf("Empty value. Setting database to localhost")
				database = "localhost"
			}
		} else if strings.Contains(a, "--debug=") {
			log.Printf("Reading debug argument: %s", a)
			stringDebug := strings.Replace(a, "--debug=", "", -1)
			debug, errParsing = strconv.ParseBool(stringDebug)
			if errParsing != nil {
				debug = false
				log.Printf("REmpty value or not explicitly true. Setting debug to false")
			}
		} else if strings.Contains(a, "--wait=") {
			pBetweenConsumeWait, errParsing := strconv.ParseFloat(strings.Replace(a, "--wait=", "", -1), 64)
			if errParsing != nil {
				log.Printf("Not a number. Setting between consuming time wait to 200 miliseconds")
				betweenConsumeWait = 2000
			} else {
				betweenConsumeWait = pBetweenConsumeWait
			}
		} else if strings.Contains(a, "--max-errors=") {
			pMaxErrorsAllowed, errParsing := strconv.ParseFloat(strings.Replace(a, "--max-errors=", "", -1), 64)
			if errParsing != nil {
				log.Printf("Not a number. Setting max errors allowed to 10")
				maxErrorsAllowed = 10
			} else {
				maxErrorsAllowed = pMaxErrorsAllowed
			}
		} else if strings.Contains(a, "--max-wait=") {
			log.Printf("Reading max time wait argument: %s ", a)
			pMaxTimeWait, errParsing := strconv.ParseFloat(strings.Replace(a, "--max-wait=", "", -1), 64)
			if errParsing != nil {
				log.Printf("Not a number. Setting max time wait to 5000 miliseconds")
				maxTimeWait = 5000
			} else {
				maxTimeWait = pMaxTimeWait
			}
		} else if strings.Contains(a, "--incremental-rate=") {
			log.Printf("Reading incremental rate argument: %s", a)
			pTimeWaitIncrementalRate, errParsing := strconv.ParseFloat(strings.Replace(a, "--incremental-rate=", "", -1), 64)
			if errParsing != nil {
				log.Printf("Not a number. Setting incremental wait to .2")
				timeWaitIncrementalRate = .2
			} else {
				timeWaitIncrementalRate = pTimeWaitIncrementalRate
			}
		}
	}

	mongoSession, errMongo = mgo.DialWithInfo(getDialInfo())
	if errMongo != nil {
		log.Fatalf("session: %s\n", errMongo)
	}
	mongoSession.SetMode(mgo.Monotonic, true)

	detailCollection = mongoSession.DB("dashboard").C("detail")
	nodeSummaryCollection = mongoSession.DB("dashboard").C("nodeSummary")
	userSummaryCollection = mongoSession.DB("dashboard").C("userSummary")
	deviceSummaryCollection = mongoSession.DB("dashboard").C("deviceSummary")

	log.Printf(" --- Runtime properties --- ")
	log.Printf("Database:%s ", database)
	log.Printf("Debug :%v ", debug)
	log.Printf("Time wait between consuming: %v", betweenConsumeWait)
	log.Printf("Max number of errors allowed:: %v", maxErrorsAllowed)
	log.Printf("Max wait time allowed: %v", maxTimeWait)
	log.Printf("Incremental rate: %f", timeWaitIncrementalRate)

}
func consumeTimeOut(timeInSecs float64) {
	log.Printf("Time in Milisecs: %f ", timeInSecs)
	timeComing := int(timeInSecs)
	timeIn := time.Duration(timeComing) * time.Millisecond
	timer := time.NewTimer(timeIn)
	<-timer.C
	runMongoProducer()
}
